## Webex Powershell Tools

This repository contains a list of smaller modules to help with specific tasks around the Webex API.

**Webex-Backup2Disk**

This tool will download all available messages and files for a user from their Webex Account.


**Webex-TeamSync**

TeamSync will synchronize a CSV file against a Webex Team or Room and update the eqivalent memberships.
Another small script will help retrieving information about an Active Directory user group for later synchronization.


**Webex-UpdateMaildomain**

Use this module to change the maildomain of all users matching a given old domain name.


**WebexAPI**

The API client used by all other modules. Can be used to develop more tools.


**WriteLog**

A little helper module used by all other modules to help with logging.
