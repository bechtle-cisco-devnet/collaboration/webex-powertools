@{
    RootModule          = 'WriteLog.psm1'
    ModuleVersion       = '1.0'
    GUID                = 'e76e1c87-a06e-49e3-8afd-c4889f41b94b'

    Description         = 'Logging Function.'
    Author              = 'Christian Drefke'

    PowerShellVersion   = '5.0'

    FunctionsToExport   = 'Write-Log'
}