function Write-Log {

    Param (
        [string] $Message,
        [string] $Level = "DEBUG"
    )

    $LoggingPath = ".\Logs"

    if (!(Test-Path $LoggingPath)) {
        New-Item -Path ".\" -Name "Logs" -ItemType "directory"
    }

    # Retrieve the name of the calling script
    $CallerScript = Get-ChildItem $MyInvocation.PSCommandPath | Select-Object -Expand Name
    $CallerName = $CallerScript.Substring(0, $CallerScript.IndexOf("."))

    # Each calling script has its own log subdirectory
    $LogDir = ".\Logs\$CallerName\"
    $FileName = "log_$(Get-Date -Format "yyyyMMdd").txt"
    $FilePath = $LogDir + $FileName

    if (!(Test-Path $LogDir)) {
        New-Item $LogDir -ItemType Directory
    }

    if (!(Test-Path $FilePath)) {
        New-Item -Path $LogDir -Name $FileName -ItemType "file"
    }

    $TimeStamp = Get-Date -Format "yyyy-MM-dd HH:mm:ss"
    $LogEntry = $TimeStamp + " " + $CallerName + " " + $Level + " " + $Message
    
    Add-Content $FilePath -value $LogEntry

    Switch ( $Level ) {
        "DEBUG" { Write-Debug ($LogEntry) }
        "INFO" { Write-Information ($LogEntry) }
        "WARNING" { Write-Warning ($LogEntry) }
        "ERROR" { Write-Error ($LogEntry) }
        "CRITICAL" { Throw $LogEntry }
    }
}

Export-ModuleMember -Function Write-Log