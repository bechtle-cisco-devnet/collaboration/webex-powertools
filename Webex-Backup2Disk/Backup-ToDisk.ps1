﻿<#
.SYNOPSIS

A script for exporting Webex chat and file data from every room the executing user is member of.

.DESCRIPTION

After selecting a target directory and entering the personal Webex Access Token, all chats and file data for the executing user will be downloaded
and stored locally.
    
For each of the Webex rooms a subdirectory is created.
    
All messages are put into a CSV file called "_messages.csv" in the target
directory.
    
All files from this room are downloaded into the same directory.

To allow the script to access the Webex data, you need to enter the personal Webex Access Token when asked to. Get the token from developer.webex.com, 
specifically from 'https://developer.webex.com/docs/api/getting-started'.

Copy the token from the Box "Your Personal Access Token". You can use the copy button next to the box to put the token into your clipboard.

BE ADVISED: Downloading the content, especially files, from large Webex rooms will take a while. Leave rooms you don't need to be part of anymore and/or
don't need to be extracted.

.LINK
https://gitlab.com/MrCollaborator/webex2disc

.EXAMPLE

PS> ./webex2disc.ps1

.INPUTS

None

.OUTPUTS

> A Directory for each Webex room from which data is exported from.
> A file name "_messages.csv" containing all chat messages. The file is located in the equivalent subdirectory for a room.
> Downloaded files from each chat room. The files are located in each rooms subdirectory.

.NOTES

File Name      : webex2disc.ps1
Author         : Christian Drefke (christian.drefke@bechtle.com)
Prerequisite   : PowerShell V5.1 (tested with 5.1, might work with lower versions)

Copyright 2021 - Christian Drefke

TROUBLESHOOTING

To see the Debug output, switch Debug preference from 'SilentlyContinue' to 'Continue':

    PS C:\> $DebugPreference = "Continue"
#>

param (
    [string]$token,
    [string]$outputdir
)

Import-Module $PSScriptRoot\..\WriteLog
Import-Module $PSScriptRoot\..\WebexApi

function getDirectory($initdir = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $dirbox = New-Object System.Windows.Forms.FolderBrowserDialog
    $dirbox.Description = "Select a folder"
    $dirbox.rootfolder = "MyComputer"
    $dirbox.SelectedPath = $initdir

    Write-Host "Select your output directory from the popup dialog."
    if ($dirbox.ShowDialog() -eq "OK") {
        $directory += $dirbox.SelectedPath
    }
    return $directory
}

function getToken {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $token = Read-Host -Prompt "Fügen Sie hier Ihren Persönlichen Token von https://developer.webex.com/docs/api/getting-started ein"
    }
    else {
        $token = Read-Host -Prompt "Paste your Personal Access Token from https://developer.webex.com/docs/api/getting-started"
    }
    return $token
}

$PSDefaultParameterValues["Export-CSV:Encoding"] = "UTF8"

if ([string]::IsNullOrEmpty($token)) { $token = getToken }
Write-Debug "Token: '$token'"

# Ask the user for an output directory through a Popup dialog.
if ([string]::IsNullOrEmpty($outputdir)) {
    $basedir = getDirectory
    Write-Debug "Output Directory: $basedir"
    if ([string]::IsNullOrEmpty($basedir)) {
        Write-Host "Cancelled"
        Break
    }
}

$client = Get-WebexClient($Token)

# Get a list of all rooms.
$rooms = $client.listRooms()

# For each room, get all messages and export them.
foreach ($room in $rooms.items) {
    Write-Host "Exporting data for room '$($room.title)'"
    $start = (Get-Date).Second

    $roomid = $room.id

    # Set the filename to be the last 10 characters of the room ID."
    $roomsuffix = $roomid.Substring($roomid.length - 10, 10)

    # Create a subdirectory for the room content
    $outdir = "$basedir\$roomsuffix"
    Write-Host "  Creating directory if not exists: $outdir"
    New-item $outdir -ItemType Directory -force | Out-Null

    $filename = "_messages.csv"
    $filepath = $outdir + "\" + $filename

    # Write the messages into CSV files. Delimiter is set to ";" and ID, RoomID as well as the RoomType are stripped from the data first.
    $messages = $client.listMessages($room.id)
    Write-Host "  Writing all room messages to $filepath"
    $messages.items | Select-Object created, updated, personEmail, text, markdown, html | Export-CSV -Path $filepath -Delimiter ";" -NoTypeInformation

    # Download all the files now.
    Write-Host "  Downloading files..."
    $filecount = 0
    foreach ($message in $messages.items) {
        if ($message.files.Count -gt 0) {
            foreach ($fileurl in $message.files) {
                $client.retrieveAttachment($fileurl, $outdir)
                $filecount++
                if ($filecount % 20 -eq 0) { Write-Host "  Filecount: $filecount" }
            }
        }
    }
    $end = (Get-Date).Second
    Write-Host "Exported roomdata in $($end - $start) seconds."
}