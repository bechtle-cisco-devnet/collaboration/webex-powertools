﻿param (
    [string]$GroupName,
    [string]$OutputFile = 'members.csv',
    [string]$RemoveHeader = $False
)

Import-Module $PSScriptRoot\..\WriteLog

function Remove-FirstLine-From-File($FilePath) {
    Write-Log "Removing first line from file: '$FilePath'." "INFO"

    $Content = Get-Content -Path $FilePath
    $Content = $Content[1..($Content.Count -1)]
    $Content | Out-File -FilePath $FilePath
}

# Remove output file if exists
if ((Test-Path -Path $OutputFile -PathType Leaf) -eq $True) {
    Remove-Item -Path $OutputFile
}

$Groups = Get-ADGroup -Filter 'Name -like "$GroupName"'

$Groups | ForEach-Object {
    $members = ''
    $GroupName = $_.Name

    $members = Get-ADGroupMember -Identity  $GroupName -Recursive | Get-ADUser -Properties Mail | Select-Object Mail
    $members | Add-Member -MemberType NoteProperty -Name Group -Value $GroupName
    $members | Export-Csv -Path $OutputFile -NoTypeInformation -Append -Encoding UTF8
}


if ($RemoveHeader -eq $True) {
    Remove-FirstLine-From-File($OutputFile)
}