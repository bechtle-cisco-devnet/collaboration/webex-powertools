﻿<#
> Run 'Get-Help .\Sync-Csv2Webex.ps1 -full' to access the content below.

.SYNOPSIS
A script for synchronizing the members of a Webex room or teams against a given CSV file.

.DESCRIPTION
After providing a CSV filename and a valid Webex Access Token, room and team memberships will synchronize against two CSV files.
Both CSV files are optional. Filename "room.csv" is used to synchronize members or a single room. Filename "teams.csv" is used to
synchronize team memberships.

The CSV column structure for both files can be found in templates 'room.csv.tpl' and 'teams.csv.tmpl'. The first line of a file will
be removed by default. You can change this behaviour with parameter -removeheader. 

The structure for 'room.csv' is simply:

| e-mail address |

And for 'teams.csv':

| e-mail address | teamname |

IMPORTANT: To protect certain accounts from getting removed of a team, add the according e-mail adresses to 'protected.csv'.
This is also necessary for your Webex Access Token user or bot.

To allow the script to access the Webex data, you need to enter a Webex Access Token when asked to. Get the token from developer.webex.com, 
specifically from 'https://developer.webex.com/docs/api/getting-started'.

Copy the token from the Box "Your Personal Access Token". You can use the copy button next to the box to put the token into your clipboard.
ATTENTION: These tokens only have a short lifetime.

To run the script more frequently and with a permanent token, you can create a bot under developer.webex.com and use the bot token. In this
case you'll also need to add the bot account to every room or team (with team moderator rights) to give it the necessary access rights.

.LINK
https://gitlab.com/bechtle-cisco-devnet/collaboration/webex-teamsync

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_ -RoomId _roomid_ -RemoveHeader $true

.PARAMETER Token
REQUIRED - A valid Webex Access Token

.PARAMETER RoomId
REQUIRED FOR ROOM SYNC - Webex API RoomID for the room you want to synchronize. Defaults to "".

.PARAMETER RemoveHeader
OPTIONAL - Option to remove the CSV header row. Defaults to $false. 

.PARAMETER RoomCsv
OPTIONAL - Name of the room CSV file. Defaults to "room.csv".

.PARAMETER TeamCsv
OPTIONAL - Name of the team CSV file. Defaults to "teams.csv".

.PARAMETER Delimiter
OPTIONAL - Delimiter for the CSV format. Needs to be identical for all CSV files.

.OUTPUTS
> Logging output to /logs.

.NOTES
File Name      : Sync-Csv2Webex.ps1
Author         : Christian Drefke (christian.drefke@bechtle.com)
Prerequisite   : PowerShell V5.1 (tested with 5.1, might work with lower versions)

Copyright 2022 - Christian Drefke

REQUIREMENTS

TODO * PowerShell version

INSTALL

The RSAT Modules are necessary to export AD information.

´Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online´

TROUBLESHOOTING

TODO * TLS 1.2

To see the Debug output, switch Debug preference from 'SilentlyContinue' to 'Continue':

    PS C:\> $DebugPreference = "Continue"
#>

param (
    [string]$Token = $env:WebexAPIToken,
    [string]$RoomId = "",
    [string]$TeamName = "",
    [bool]$RemoveHeader = $False,
    [string]$RoomCsv = "room.csv",
    [string]$TeamsCsv = "teams.csv",
    [string]$Delimiter = ","
)

Import-Module $PSScriptRoot\..\WriteLog
Import-Module $PSScriptRoot\..\WebexApi

# Activate Informational Console Output
$InformationPreference = "Continue";

# Activate Debug Console Output
$DebugPreference = "Continue";

function Get-Directory($initdir = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $dirbox = New-Object System.Windows.Forms.FolderBrowserDialog
    $dirbox.Description = "Select a folder"
    $dirbox.rootfolder = "MyComputer"
    $dirbox.SelectedPath = $initdir

    Write-Log "Select your output directory from the popup dialog." "INFO"
    if ($dirbox.ShowDialog() -eq "OK") {
        $directory += $dirbox.SelectedPath
    }
    return $directory
}

function Remove-FirstLine-From-File($FilePath) {
    Write-Log "Removing first line from file: '$FilePath'." "INFO"

    $Content = Get-Content -Path $FilePath
    $Content = $Content[1..($Content.Count -1)]
    $Content | Out-File -FilePath $FilePath
}

function Sync-LeftToRight {

    param (
        [ValidateNotNullOrEmpty()]
        [System.Array]$membersSource,

        [ValidateNotNullOrEmpty()]
        [System.Array] $membersTarget,

        [string] $roomOrTeam = "room"
    )
    $membersCompared = Compare-Object -ReferenceObject $membersSource.email -DifferenceObject $membersTarget.personEmail | Where-Object SideIndicator -eq "<="
    # DEBUG
    # Write-Host ($membersCompared.InputObject | Format-Table | Out-String)

    Write-Log "Found $($membersCompared.Count) accounts for ADD jobs." "INFO"

    foreach ($Member in $membersCompared) {
        if ($roomOrTeam -eq "room") {
            $Member = $Member.InputObject
        
            $client.AddMembership($RoomId, $Member)
        }
        elseif ($roomOrTeam -eq "team") {
            $client.AddTeamMembership($teamId, $Member.InputObject)
        }
        else {
            Write-Log "Parameter 'roomOrTeam' must be either 'room' or 'team'." "ERROR"
        }
    }
}

function Sync-RightToLeft {

    Param (
        [ValidateNotNullOrEmpty()]
        [System.Array]$membersSource,

        [ValidateNotNullOrEmpty()]
        [System.Array] $membersTarget,

        [string] $roomOrTeam = "room"
    )
    # Add protected accounts to the Source Liste
    try {
        $protected = Import-Csv -Path .\protected.csv -Delimiter "," -Header 'email'
        $membersSource = $membersSource + $protected        
    }
    catch {
        Write-Log "No file '.\protected.csv'. Processing withough protected accounts." "INFO"
    }

    $membersCompared = Compare-Object -ReferenceObject $membersSource.email -DifferenceObject $membersTarget.personEmail | Where-Object SideIndicator -eq "=>"
    # DEBUG
    # Write-Host ($membersCompared.InputObject | Format-Table | Out-String)
    
    Write-Log "Found $($members.Compared.Count) accounts for REMOVE jobs." "INFO"

    foreach ($member in $membersCompared) {
        if ($roomOrTeam -eq "room") {
            $membership = $membersTarget | Where-Object personEmail -eq $member.InputObject
            # DEBUG
            # Write-Host ($membership.id)
    
            $client.RemoveMembership($membership.id)
        }
        elseif ($roomOrTeam -eq "team") {
            $membership = $membersTarget | Where-Object personEmail -eq $member.InputObject
            # DEBUG
            # Write-Host ($membership.id)
    
            $client.RemoveTeamMembership($membership.id)
        }
        else {
            Write-Log "Parameter 'roomOrTeam' must be either 'room' or 'team'." "CRITICAL"
        }
    }
}

function Sync-Room {

    Param (
        [ValidateNotNullOrEmpty()]
        [string] $CsvFile,
        
        [ValidateNotNullOrEmpty()]
        [string] $RoomId,

        [bool] $RemoveHeader = $False,
        [string] $Delimiter = ","
    )
    Write-Log "Synchronizing room CSV '$CsvFile'." "INFO"
    
    # Initiate the Webex API Client
    $client = Get-WebexClient($Token)
    
    # Import Active Directory Member data after deleting the header row
    $Header = 'email'
    
    if ($RemoveHeader -eq $True) {
        Remove-FirstLine-From-File($CsvFile)
    }
    $membersImport = Import-Csv -Path $CsvFile -Delimiter $Delimiter -Header $Header
    Write-Log "$($membersImport.Count) entries found in CSV file."

    # DEBUG
    # Write-Host ($membersImport | Format-Table | Out-String)

    # Only valid for named imports
    # $membersSource = $membersImport | Where-Object name -eq $_ | Select-Object email
    $membersSource = $membersImport | Select-Object email
    # DEBUG
    # $membersSource | Get-Member

    # DEBUG
    # Write-Host ($teamsReference | Format-Table | Out-String)

    Write-Log "Processing Rooom ID '$RoomId'" "INFO"

    $membersTarget = $client.GetMemberships($RoomId)
    $membersTarget = $membersTarget.items | Select-Object -Property id, personEmail

    # DEBUG
    # $membersTarget | Get-Member

    ### Import Users who are not yet members of the Room. ###
    Sync-LeftToRight $membersSource $membersTarget
    ###########################################################

    ### Delete Memberships of users who are in the Source List anymore. ###
    Sync-RightToLeft $membersSource $membersTarget
    ###########################################################
}

function Sync-Teams {

    Param (
        [ValidateNotNullOrEmpty()]
        [string] $CsvFile,

        [string] $TeamName = "",
        [bool] $RemoveHeader = $False,
        [string] $Delimiter = ","
    )
    Write-Log "Synchronizing team CSV '$CsvFile'." "INFO"

    # Initiate the Webex API Client
    $client = [Webex]::new($token)

    # Import Active Directory Member data after deleting the header row
    $Header = 'email', 'name'
    
    if ($RemoveHeader -eq $True) {
        Remove-FirstLine-From-File($CsvFile)
    }
    $membersImport = Import-Csv -Path $CsvFile -Delimiter $Delimiter -Header $Header
    Write-Log "$($membersImport.Count) entries found in CSV file."
    # DEBUG
    # Write-Host ($membersImport | Format-Table | Out-String)

    $teamsReference = $membersImport.name | Select-Object -Unique
    # DEBUG
    # Write-Host ($teamsReference | Format-Table | Out-String)
    
    if ($teamName) {
        # Get the Team ID if name was given.
        $teamId = $client.GetTeamByName($teamName)
    }

    $teams = $client.GetTeams()
    
    if ($teams.items.Count -eq 0) {
        Write-Log "No access to any teams. Skipping team synch."
        return
    }
    
    $teamsCompared = Compare-Object -ReferenceObject $teamsReference -DifferenceObject $teams.items.name -IncludeEqual | Where-Object SideIndicator -eq "=="
    # DEBUG
    # Write-Host ($teamsCompared.InputObject | Format-Table | Out-String)
    
    $teamsCompared.InputObject | ForEach-Object {
        Write-Log "Synchronizing team '$_'." "INFO"

        $teamId = $teams.items | Where-Object name -eq $_
        $teamId = $teamId.id
        
        # DEBUG
        # Write-Host $teamId

        $membersSource = $membersImport | Where-Object name -eq $_ | Select-Object email 
        # DEBUG
        # $membersSource | Format-Table | Out-String
        
        $membersTarget = $client.GetTeamMemberships($teamId)
        $membersTarget = $membersTarget.items | Select-Object -Property id, personEmail

        # DEBUG
        # $membersTarget | Get-Member
        
        ### Import Users who are not yet members of the Team. ###
        Sync-LeftToRight $membersSource $membersTarget "team"
        ###########################################################

        ### Delete Memberships of users who are in the Source List anymore. ###
        Sync-RightToLeft $membersSource $membersTarget "team"
        ###########################################################
    }
}

Invoke-Command -ScriptBlock {
    Write-Log "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    # DEBUG AND DEV ONLY
    # $token = ""
    # $teamId = ""
    # $teamId = ""
    # $RoomId = ""

    if ((Test-Path -Path $RoomCsv -PathType Leaf) -eq $True) {
        if ($RoomId) {
            Sync-Room -CsvFile $RoomCsv -RoomId $RoomId -RemoveHeader $RemoveHeader
        }
        else {
            Write-Log "CSV file '$RoomCsv' found but Room ID is missing. Skipping room synch." "WARNING"
        }
    }
    else {
        Write-Log "CSV file '$RoomCsv' does not exist. Skipping room synch." "INFO"
    }

    if ((Test-Path -Path $TeamsCsv -PathType Leaf) -eq $True) {
        Sync-Teams -CsvFile $TeamsCsv -TeamName $TeamName -RemoveHeader $RemoveHeader
    }
    else {
        Write-Log "CSV file '$RoomCsv' does not exist. Skipping team synch." "INFO"
    }
}