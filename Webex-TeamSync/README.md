## Getting Started

1. Login to developer.webex.com
2. Create a Webex Bot and save the Webex Bot Access Token and Address to your password safe.
3. Add the Bot to the room or team you wand to synchronize.
4. Under the developer.webex.com API documentation, go to "Messages/Rooms"
5. "List Rooms", add your Bot token as Bearer token and Submit.
6. Write down your room ID and save it somewhere locally.
7. For AD synch, run "Export-ADGroupMembers2Csv.ps1" first.

In Powershell
```
.\Export-AdGroupMembers2Csv.ps1 -GroupName "<NAME OF AD GROUP>" -RemoveHeader $True -OutputFile "<NAME OF AD GROUP>.csv"
```

8. Run Sync-Csv2Webex.ps1 for the synch.

In Powershell
```
.\Sync-Csv2Webex.ps1 -Token "<BOT ACCESS TOKEN>" -RoomId "<ROOM ID>" -RoomCsv "<NAME OF AD GROUP>.csv"
```

That's it.

You can run these two jobs scheduled through Windows Task Scheduler.