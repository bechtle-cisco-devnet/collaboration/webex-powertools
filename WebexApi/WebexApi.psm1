Import-Module .\WriteLog

function Get-Token {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $token = Read-Host -Prompt "Fügen Sie hier Ihren Persönlichen Token von https://developer.webex.com/docs/api/getting-started ein"
    }
    else {
        $token = Read-Host -Prompt "Paste your Personal Access Token from https://developer.webex.com/docs/api/getting-started"
    }
    return $token
}

class WebexApi {
    [string] $Protocol = 'https'
    [string] $Hostname = 'webexapis.com'
    [string] $Port = '443'
    [string] $ApiVersion = '1'
    hidden [string] $Token

    WebexApi() {}

    WebexApi(
        [string]$Token) {
        # Ask the user for a valid Webex token if not provided through parameters.
        if ([string]::IsNullOrEmpty($Token)) {
            $this.Token = Get-Token
        }
        
        $this.Token = $Token
        # DEBUG
        # Write-Debug "Token: '$($this.Token)'"
    }

    [PSCustomObject] AddMembership([string] $RoomId, [string] $personEmail) {
        Write-Log "Adding new member '$personEmail' to room id '$RoomId'." "INFO"

        $endpoint = "/memberships"
        $params = $this.GetParams($endpoint, "POST")
        $body = @{
            roomId = $RoomId
            personEmail = $personEmail
        }
        $jsonbody = $body | ConvertTo-Json

        $params.ContentType = "application/json"
        $params.Body = $jsonbody

        $result = $this.SendRequest($params, $true)

        Write-Log "Room membership $($result.id) created." "INFO"
        
        return $result
    }

    [PSCustomObject] AddTeamMembership([string] $teamId, [string] $personEmail) {
        Write-Log "Adding teammember '$personEmail' to team '$teamId'." "INFO"

        $endpoint = "/team/memberships"
        $params = $this.GetParams($endpoint, "POST")
        $body = @{
            teamId = $teamId
            personEmail = $personEmail
        }
        $jsonbody = $body | ConvertTo-Json

        $params.ContentType = "application/json"
        $params.Body = $jsonbody

        $result = $this.SendRequest($params, $true)

        Write-Log "Team membership $($result.id) created." "INFO"
        
        return $result
    }

    [void] GetAttachment($fileurl, $directory) {

        $endpoint = ""
        $params = $this.GetParams($endpoint, "HEAD")
        $params.Uri = $fileurl
        $result = $this.SendRequest($params, $false)

        #Write-Debug $result["Content-Disposition"]
        $filename = [regex]::matches($result["Content-Disposition"], '(?<=\").+(?=\")').value
        #Write-Debug "Filename: $filename"

        $params.Method = "GET"
        $params.OutFile = $directory + "/" + $filename

        $this.SendApiRequest($params)
    }

    [PSCustomObject] GetMe() {
        Write-Log "Getting information about Me." "INFO"

        $endpoint = "/people/me"
        $method = "GET"
        $params = $this.GetParams($endpoint, $method)

        return $this.SendRequest($params, $false)
    }

    [PSCustomObject] GetMemberships($RoomId) {
        Write-Log "Getting memberships for room '$RoomId'" "INFO"

        $endpoint = "/memberships/?max=1000&roomId=" + $RoomId
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of room members in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetMessages($room) {
        Write-Log "Getting all room messages. This can take a moment for large rooms." "INFO"
        $endpoint = "/messages/?RoomId=$room&max=1000"

        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)
        $m = $result.items | Measure-Object

        Write-Log "Number of messages in room: $($m.Count)" "INFO"
        
        return $result
    }

    [hashtable] GetParams([string] $endpoint, [string] $method) {
        $url = $this.Protocol + "://" + $this.Hostname + ":" + $this.Port + "/v" + $this.ApiVersion + $endpoint
        $headers = @{ "Authorization" = "Bearer $($this.token)" }

        $params = @{
            Uri     = $url
            Headers = $headers
            Method  = $method
        }
        return $params
    }

    [PSCustomObject] GetRooms() {
        Write-Log "Getting list of rooms." "INFO"

        $endpoint = "/rooms/?sortBy=lastactivity&max=1000"
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of rooms in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetTeamByName([string] $name) {
        # Get a team Id by searching for the team name.
        Write-Log "Getting ID of team '$name'" "INFO"

        $teams = $this.GetTeams()

        # DEBUG
        # Write-Host ($teams.items | Format-Table | Out-String)

        $filtered = $teams.items | Where-Object {$_.name -eq $teamName}
        if ($filtered) {
            $result = $filtered.id
        } else {
            Write-Log "Team '$name' not found." "INFO"
            $result = $null
        }
        return $result
    }

    [PSCustomObject] GetTeamMemberships($teamId) {
        Write-Log "Getting team memberships." "INFO"

        $endpoint = "/team/memberships/?max=1000&teamId=" + $teamId
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of teammembers in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] GetTeams() {
        Write-Log "Getting list of teams." "INFO"

        $endpoint = "/teams/?max=1000"
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of teams in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] ListPeople() {
        Write-Log "Getting list of people." "INFO"

        $endpoint = "/people/?max=1000"
        $params = $this.GetParams($endpoint, "GET")
        $result = $this.SendRequest($params, $true)

        $m = $result.items | Measure-Object

        Write-Log "Number of people in list: $($m.Count)" "INFO"
        
        return $result
    }

    [PSCustomObject] RemoveMembership([string] $MembershipId) {
        Write-Host "Removing room membership '$MembershipId'." "INFO"

        $endpoint = "/memberships/" + $MembershipId
        $params = $this.GetParams($endpoint, "DELETE")
        $result = $this.sendRequest($params, $true)

        Write-Log "Room membership '$MembershipId' deleted." "INFO"
        
        return $result
    }

    [PSCustomObject] RemoveTeamMembership([string] $MembershipId) {
        Write-Log "Removing team membership '$MembershipId'." "INFO"

        $endpoint = "/team/memberships/" + $MembershipId
        $params = $this.GetParams($endpoint, "DELETE")
        $result = $this.sendRequest($params, $true)

        Write-Log "Team membership $MembershipId deleted." "INFO"

        return $result
    }

    [PSCustomObject] UpdatePeople([string] $PeopleId, [PSCustomObject] $data) {
        Write-Log "Updating Person Id '$PeopleId'." "INFO"

        $endpoint = "/people/$PeopleId"
        $params = $this.GetParams($endpoint, "PUT")

        $body = $data

        $jsonbody = $body | ConvertTo-Json

        # DEBUG
        # Write-Log $jsonbody

        $params.ContentType = "application/json"
        $params.Body = $jsonbody

        $result = $this.SendRequest($params, $true)

        Write-Log "Person $($result.id) updated." "INFO"
        
        return $result
    }

    [PSCustomObject] SendRequest([hashtable] $params, [bool] $paged = $false) {
        $response = $this.SendApiRequest($params)
        if ($params.Method -eq "HEAD") {
            $result = $response.Headers
            return $result
        }
        $result = $response.Content | ConvertFrom-Json

        if ($paged -eq $true) {
            $linkheader = $response.Headers.Link
            while ($linkheader -match '<(?<Link>.+)>; rel="next"') {
                $params.Uri = $Matches.Link
                $response = $this.SendApiRequest($params)
                $temp = $response.Content | ConvertFrom-Json
                #$m = $temp.items | measure
                #Write-Debug "Number of temp items: $($m.Count)"
                $result.items += $temp.items

                $linkheader = $response.Headers.Link
            }
            return $result
        }
        else {
            return $result
        }
    }

    [Object] SendApiRequest($params) {
        try {
            Write-Log "Sending Web Request with Params: uri=$($params.Uri) method=$($params.Method)"
            if ($params.Body) {
                Write-Log "Sending $($params.Method) data: $($params.Body)"
            }
            $response = Invoke-WebRequest @params

            # Little hack cause $response will be empty if -OutFile is in Webrequest
            if ($params.ContainsKey("OutFile")) { response = @{} }

            return $response
        }
        catch {
            $Statuscode = $_.Exception.Response.StatusCode.value__
            $Message = $_.Exception.Message
            Write-Log "$Statuscode - $Message" "DEBUG"

            Switch ( $Statuscode) {
                "400" { Write-Log "400 - Request Failed." "ERROR" }
                "401" { Write-Log "401 - Authentication failed. Provide a valid token." "CRITICAL" }
                "404" { Write-Log "404 - Endpoint Url $($params.Uri) does not exist." "ERROR" }
                "405" { Write-Log "405 - Method $($params.Method) is not allowed." "ERROR" }
                "409" { Write-Log "409 - There was a conflict sending the request. The target might already exist." "ERROR" }
                "423" {
                    $waitfor = $_.Exception.Response.Headers["Retry-After"]
                    Write-Log "Resource is temporarily unavailable, will have to wait for $waitfor seconds." "INFO"
                    Start-Sleep -s $waitfor
                    $this.SendApiRequest($params)
                }
                "429" {
                    $waitfor = $_.Exception.Response.Headers["Retry-After"]
                    Write-Log "Too many requests, will have to wait for $waitfor seconds." "INFO"
                    Start-Sleep -s $waitfor
                    $this.SendApiRequest($params)
                }
                "500" { Write-Log "Target system is experiencing an error. Try again later." "ERROR" }
                "503" { Write-Log "Target system is overloaded. Try again later." "ERROR" }
            }
            Write-Log ("Web Response Status Code: $statuscode")
            return @{"statuscode" = $statuscode }
        }
    }
}

function Get-WebexClient {
    Param (
        [string] $Token
    )

    return [WebexApi]::new($token)
}

Export-ModuleMember -Function Get-WebexClient