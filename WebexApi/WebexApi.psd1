@{
    RootModule          = 'WebexApi.psm1'
    ModuleVersion       = '0.3.0'
    GUID                = 'ef7b661b-bf20-4898-9fba-a18dc37bdd9f'

    Description         = 'A WebexAPI client for Powershell.'
    Author              = 'Christian Drefke'

    PowerShellVersion   = '5.0'

    FunctionsToExport   = 'Get-WebexClient'
}