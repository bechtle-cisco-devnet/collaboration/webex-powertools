﻿<#
> Run 'Get-Help .\Sync-Csv2Webex.ps1 -full' to access the content below.

.SYNOPSIS
A script for synchronizing the members of a Webex room or teams against a given CSV file.

.DESCRIPTION
After providing a CSV filename and a valid Webex Access Token, room and team memberships will synchronize against two CSV files.
Both CSV files are optional. Filename "room.csv" is used to synchronize members or a single room. Filename "teams.csv" is used to
synchronize team memberships.

The CSV column structure for both files can be found in templates 'room.csv.tpl' and 'teams.csv.tmpl'. The first line of a file will
be removed by default. You can change this behaviour with parameter -removeheader. 

The structure for 'room.csv' is simply:

| e-mail address |

And for 'teams.csv':

| e-mail address | teamname |

IMPORTANT: To protect certain accounts from getting removed of a team, add the according e-mail adresses to 'protected.csv'.
This is also necessary for your Webex Access Token user or bot.

To allow the script to access the Webex data, you need to enter a Webex Access Token when asked to. Get the token from developer.webex.com, 
specifically from 'https://developer.webex.com/docs/api/getting-started'.

Copy the token from the Box "Your Personal Access Token". You can use the copy button next to the box to put the token into your clipboard.
ATTENTION: These tokens only have a short lifetime.

To run the script more frequently and with a permanent token, you can create a bot under developer.webex.com and use the bot token. In this
case you'll also need to add the bot account to every room or team (with team moderator rights) to give it the necessary access rights.

.LINK
https://gitlab.com/bechtle-cisco-devnet/collaboration/webex-teamsync

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_

.EXAMPLE
PS> ./Sync-Csv2Webex.ps1 -Token _token_ -RoomId _roomid_ -RemoveHeader $true

.PARAMETER Token
REQUIRED - A valid Webex Access Token

.PARAMETER RoomId
REQUIRED FOR ROOM SYNC - Webex API RoomID for the room you want to synchronize. Defaults to "".

.PARAMETER RemoveHeader
OPTIONAL - Option to remove the CSV header row. Defaults to $false. 

.PARAMETER RoomCsv
OPTIONAL - Name of the room CSV file. Defaults to "room.csv".

.PARAMETER TeamCsv
OPTIONAL - Name of the team CSV file. Defaults to "teams.csv".

.PARAMETER Delimiter
OPTIONAL - Delimiter for the CSV format. Needs to be identical for all CSV files.

.OUTPUTS
> Logging output to /logs.

.NOTES
File Name      : Sync-Csv2Webex.ps1
Author         : Christian Drefke (christian.drefke@bechtle.com)
Prerequisite   : PowerShell V5.1 (tested with 5.1, might work with lower versions)

Copyright 2022 - Christian Drefke

REQUIREMENTS

TODO * PowerShell version

INSTALL

The RSAT Modules are necessary to export AD information.

´Get-WindowsCapability -Name RSAT* -Online | Add-WindowsCapability -Online´

TROUBLESHOOTING

TODO * TLS 1.2

To see the Debug output, switch Debug preference from 'SilentlyContinue' to 'Continue':

    PS C:\> $DebugPreference = "Continue"
#>

param (
    [string]$Token = $env:WebexAPIToken,
    [string]$DomainOld = "",
    [string]$DomainNew = ""
    )

Import-Module $PSScriptRoot\..\WriteLog
Import-Module $PSScriptRoot\..\WebexApi

# Activate Informational Console Output
$InformationPreference = "Continue";

# Activate Debug Console Output
$DebugPreference = "Continue";

function Get-Directory($initdir = "") {
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

    $dirbox = New-Object System.Windows.Forms.FolderBrowserDialog
    $dirbox.Description = "Select a folder"
    $dirbox.rootfolder = "MyComputer"
    $dirbox.SelectedPath = $initdir

    Write-Log "Select your output directory from the popup dialog." "INFO"
    if ($dirbox.ShowDialog() -eq "OK") {
        $directory += $dirbox.SelectedPath
    }
    return $directory
}

function Get-DomainNew {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $name = Read-Host -Prompt "Name der neuen Domäne: "
    }
    else {
        $name = Read-Host -Prompt "Name of the new domain name: "
    }
    return $name
}

function Get-DomainOld {
    $lang = Get-WinSystemLocale

    if ($lang.Name -eq "de-DE") {
        $name = Read-Host -Prompt "Name der alten Domäne: "
    }
    else {
        $name = Read-Host -Prompt "Name of the old domain name: "
    }
    return $name
}

function Update-Maildomain {

    Param (
        [string] $Token,

        [ValidateNotNullOrEmpty()]
        [string] $DomainOld,

        [ValidateNotNullOrEmpty()]
        [string] $DomainNew
    )
    Write-Log "Updating People Mail Domain from '$DomainOld' to '$DomainNew'." "INFO"

    # Initiate the Webex API Client
    $client = Get-WebexClient($Token)

    $peopleList = $client.ListPeople()
    # DEBUG
    # Write-Host ($peopleList | Format-Table | Out-String)

    if ($peopleList.items.Count -eq 0) {
        Write-Log "No access to any people. Skipping domain update."
        return
    }

    [System.Collections.ArrayList]$matchList= @()

    $peopleList.items | ForEach-Object {
        If ($_.emails.EndsWith($DomainOld)) {
            Write-Host "E-Mail is matching."
            $info = [PSCustomObject]@{
                Id = $_.id
                PersonData = $_
            }
            $matchList.add($info)
        }
    }

    $matchList | ForEach-Object {
        $NewMail = $_.PersonData.emails -replace $DomainOld, $DomainNew
        Write-Host "Updated e-mail from '$($_.PersonData.emails)' to '$NewMail'"

        $_.PersonData.emails = $NewMail

        $client.UpdatePeople($_.Id, $_.PersonData)
    }
}

Invoke-Command -ScriptBlock {
    Write-Log "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
    
    # DEBUG AND DEV ONLY
    # $Token = ""
    # $DomainOld = ""
    # $DomainNew = ""

    if ([string]::IsNullOrEmpty($DomainOld)) {
        $DomainOld = Get-DomainOld
    }
    if ([string]::IsNullOrEmpty($DomainNew)) {
        $DomainNew = Get-DomainNew
    }

    Update-Maildomain $Token $DomainOld $DomainNew
}